#----------------------- Question 1 ------------------------
'''
In Scrabble1 each player has a set of tiles with letters on them. 
The object of the game is to use those letters to spell words.
The scoring system is complex, but longer words are usually
worth more than shorter words.

Imagine you are given your set of tiles as a string,
like "quijibo", and you are given another string to test, like "jib".

Write a logic that takes two strings and checks whether the set of tiles 
can spell the word. You might have more than one tile with the same letter, 
but you can only use each tile once.
'''


tiles_string = input("Tiles_string: ")
user_string = input("User_string: ")
tiles_in_list = list(tiles_string)
counter=0
for letter in user_string:
    if letter in tiles_in_list:
        tiles_in_list.remove(letter)
        counter += 1
    else:
        print("False")
        break
if counter == len(user_string):
    print("True") 


#-----------------Question 2 ------------
'''
We have a string s, s = '12345' * 5. 
Produce following ouput by using string slicing:
1. '11111'
2. '55555'
3. Reverse the string
'''


s = input("Enter string \n")
full_sting = s*5
length = len(s)
print(full_sting[::length])
print(full_sting[length-1::length])
print(full_sting[::-1])


#---------------Question 3------------------
'''
Write a Python program to construct the following pattern, using a nested for loop.

* 
* * 
* * * 
* * * * 
* * * * * 
* * * * 
* * * 
* * 
*
'''


num = int(input("Enter the number\n"))
for i in range(num):
    for j in range(i):
        print("*", end = "")
    print()
    
for i in range(num-1):
    for j in range(num-2-i):
        print("*", end = "")
    print()